# -*- coding: utf-8 -*-
import math
import json
from datetime import date
import dateutil.parser

import pandas as pd
import flask
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
from plotly import graph_objs as go

from func.TW_func import *
from app import app, indicator, indicatorValue, TweetDisplay

#path of file to analyse
fileAirFrance = 'DataProcessed/TableTweetsAirFranceTraite.csv'




layout = [

    # top controls
    html.Div(   #Dropdown button
        [
            html.Div(
                dcc.Dropdown(
                    id="socialNetworkDropdown",
                    options=[
                        {"label": "Twitter", "value": "TW"},
                        {"label": "Facebook", "value": "FB"},
                        {"label": "Youtube", "value": "YT"},
                    ],
                    value="TW",
                    clearable=False,
                ),
                className="two columns",
            ),

        ],
        className="row",
        style={"marginBottom": "10"},
    ),

##########################
    #indicators row
##########################


    html.Div(
        [
            TweetDisplay(   #Display the most popular Tweet
                "#032653",
                "Top Message",
                "TopThing",
                str(TweetLePlusPopulaire(fileAirFrance)[2]),
            ),
        ],
        className="row",
        style = {'width':'95%', 'fontSize': '8px', 'height':'20%'},
    ),

    html.Div(
        [
            html.Div(         #Display the emotion bar chart according to the dropdown
                [
                    dcc.Graph(          
                        id="TopThingEmotions",
                        figure={
                            'data': [
                                {'x': [1], 'y': [1.46871776464786], 'type': 'bar', 'name': 'Anger'},
                                {'x': [1], 'y': [3.09234031207392], 'type': 'bar', 'name': 'Disgust'},
                                {'x': [1], 'y': [2.06783599555953], 'type': 'bar', 'name': 'Fear'},
                                {'x': [1], 'y': [7.34083555412328], 'type': 'bar', 'name': 'Joy'},
                                {'x': [1], 'y': [1.7277074477352], 'type': 'bar', 'name': 'Sadness'},
                                {'x': [1], 'y': [2.78695866252273], 'type': 'bar', 'name': 'Surprise'},
            ],
            'layout': {
                'title': 'Top Tweet Emotions'}
                        },
                    ),
                ],
                className="four columns chart_div",
                style = {"marginTop": "5px",'width':'45%'}
                ),

            html.Div(        #Display the wordcloud according to the dropdown
                [
                html.Img(id='wordcloud',src='https://i.postimg.cc/SRjvw0hp/word-Cloud-Air-France.png',height="80%",style={"float":"left","height":"80%","marginTop": "6px"}),
                ],className = "eight columns charts_div",
                style = {'width':'45%'}
                ),

        ],
        className="row",
    )
]


@app.callback(                          #Permits changing the top comment/tweet
    Output("TopThing", "children"),
    [Input("socialNetworkDropdown", "value")],
)
def selectDisplay(network):
    if network == "TW":
        return str(TweetLePlusPopulaire(fileAirFrance)[2])
    elif network == "YT":
        return "Terrible. Not flying again."
    elif network == "FB":
        return "Extraordinary plane for work travel I have fond memories."

@app.callback(                      #Permits changing the data in the bar chart
    Output("TopThingEmotions", "figure"),
    [Input("socialNetworkDropdown", "value")],
)
def selectData(network):
    if network == "TW":
        return {'data': [{'x': [1], 'y': [1.46871776464786], 'type': 'bar', 'name': 'Anger'},{'x': [1], 'y': [3.09234031207392], 'type': 'bar', 'name': 'Disgust'},{'x': [1], 'y': [2.06783599555953], 'type': 'bar', 'name': 'Fear'},{'x': [1], 'y': [7.34083555412328], 'type': 'bar', 'name': 'Joy'},{'x': [1], 'y': [1.7277074477352], 'type': 'bar', 'name': 'Sadness'},{'x': [1], 'y': [2.78695866252273], 'type': 'bar', 'name': 'Surprise'},]}
    elif network == "YT":
        return {'data': [{'x': [1], 'y': [1.46], 'type': 'bar', 'name': 'Anger'},{'x': [1], 'y': [3.09234031207392], 'type': 'bar', 'name': 'Disgust'},{'x': [1], 'y': [7.34], 'type': 'bar', 'name': 'Fear'},{'x': [1], 'y': [1.02], 'type': 'bar', 'name': 'Joy'},{'x': [1], 'y': [1.72], 'type': 'bar', 'name': 'Sadness'},{'x': [1], 'y': [2.78], 'type': 'bar', 'name': 'Surprise'},]}
    elif network == "FB":
        return {'data': [{'x': [1], 'y': [1.46], 'type': 'bar', 'name': 'Anger'},{'x': [1], 'y': [2.1], 'type': 'bar', 'name': 'Disgust'},{'x': [1], 'y': [2.06], 'type': 'bar', 'name': 'Fear'},{'x': [1], 'y': [5.64], 'type': 'bar', 'name': 'Joy'},{'x': [1], 'y': [1.73], 'type': 'bar', 'name': 'Sadness'},{'x': [1], 'y': [1.2], 'type': 'bar', 'name': 'Surprise'},]}


@app.callback(                #Permits changing the wordcloud (hosted in line)
    Output("wordcloud", "src"),
    [Input("socialNetworkDropdown", "value")],
)
def selectDisplay(network):
    if network == "TW":
        return "https://i.postimg.cc/SRjvw0hp/word-Cloud-Air-France.png"
    elif network == "YT":
        return "https://i.postimg.cc/nVvXB3dw/wordcloud-AFYTpetit.png"
    elif network == "FB":
        return "https://i.postimg.cc/1RLLhHsP/wordcloud-AFFBpetit.png"


