# -*- coding: utf-8 -*-
import json
import math

import pandas as pd
import flask
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
from plotly import graph_objs as go
import numpy as np
from func.TW_func import *
from app import app, indicator, indicatorValue, TweetDisplay #, #millify, df_to_table, sf_manager, 

fileAirFrance = 'DataProcessed/TableTweetsAirFranceTraite.csv'


polarity = TweetLePlusPopulaire('DataProcessed/TableTweetsAirFranceTraite.csv')[9]



layout = [


    
    html.Div(
        [
            TweetDisplay(
                "#032653",
                "Top Tweet",
                "TopTweet",
                str(TweetLePlusPopulaire(fileAirFrance)[2]),
            ),

            TweetDisplay(
                "#032653",
                "Polarité du top Tweet Airfrance",
                "polarity",
                str(TweetLePlusPopulaire(fileAirFrance)[9]),
            )






        ],
        className="row",
       	style = {'color':'green'}
    ),

    # charts row div
    html.Div(
        [
            

            html.Div(

        [
            TweetDisplay(
                "#032653",
                "Mentionné 26 fois avec Lufthansa",
                "mention1",
                "y’all got deals for black friday?????? @Delta @airfrance @AmericanAir @united @lufthansa",
            ),

        ],

        className="row",
        style = {'color':'red'}

    ),
            html.Div(

        [
            TweetDisplay(
                "#032653",
                "Mentionné 24 fois avec British Airways",
                "mention2",
                "Will we ever see the likes of #concorde again? Such power, speed and beauty. Nothing can match what she achieved https://www.amazon.co.uk/Concorde-Timelines-History-Supersonic-Airliner/dp/0993095097/ … #avgeek #britishairways #airfrance",
            ),


        ],

        className="row",
        style = {'color':'red'}


    ),

            html.Div(

        [
            dcc.Graph(
                id='alerts_followers_compet',
                figure={
                    'data': [
                    	go.Pie(labels=['AirFrance', 'British Airways', 'Lufthansa'],
                    		values=[int(NumberOfFollowers('airfrance')),int(NumberOfFollowers('British_Airways')),int(NumberOfFollowers('lufthansa'))], 
                    		marker=dict(colors=['#032653','#DF0101','#f0af00'])
                    		)
                    	],
                    'layout': {
                        'title': 'Attention à votre audience sur Twitter : vous êtes les moins suivis parmi vos concurrents directs. '
                    }
                }
            ),
                
            

        ],

        className="row",


    )
        ],
        className="row",
        style={"marginTop": "5"},
    )
    ]