# -*- coding: utf-8 -*-
import math
import json

import pandas as pd
import flask
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
from plotly import graph_objs as go

from app import app, indicator, indicatorValue#, millify, df_to_table, sf_manager

###### Social media functions

from func.YT_func import *
from func.FB_func import *

###### Twitter functions
from func.TW_func import *

colors = {"background": "#FFFFFF", "background_div": "white"}


airFrance_ID=106062176
temp_country_file='DataProcessed/countries.tmp'

file = 'DataProcessed/TableTweetsAirFranceTraite.csv'
temp_sent_FB_file = 'DataProcessed/sent_FB.tmp'


layout = [


    # top controls
    html.Div(
        [
            html.Div(
                dcc.Dropdown(
                    id="network_dropdown",
                    options=[
                        {"label": "Global", "value": "G"},
                        {"label": "Youtube", "value": "YT"},
                        {"label": "Twitter", "value": "TW"},
                        {"label": "Facebook", "value": "FB"},
                    ],
                    value="G",
                    clearable=False,
                ),
                className="two columns",
                style={"marginBottom": "10"},
            ),
            
        ],
        className="row",
        style={},
    ),


########################
    # INDICATEURS #
########################

    html.Div(
        [
            indicatorValue(
                "#00cc96",
                "Abonnés Youtube",
                "YT_sub",
                NombresPropres(str(return_stats(1)[1])),
            ),

            indicatorValue(
                "#119DFF",
                "Followers Twitter",
                "TW_fol",
                NombresPropres(str(NumberOfFollowers('airfrance'))),
            ),
            indicatorValue(
                "#EF553B",
                "Likes Facebook",
                "FB_like",
                "7 421 363",
            ),
        ],
        className="row",
    ),

    # indicators div 
    html.Div(
        [
            indicatorValue(
                "#00cc96",
                "Vues Youtube",
                "YT_view",
                NombresPropres(str(return_stats(1)[0])),
            ),

            indicatorValue(
                "#119DFF",
                "Nombre de fils d'actualités dans lesquels vous êtes mentionnés",#(Visibilité )
                "TW_retwt",
                NombresPropres(str(TotalVisibilité('DataProcessed/TableTweetsAirFranceTraite.csv'))),

            ),
            indicatorValue(
                "#EF553B",
                "Avis Facebook",
                "YT_opinion",
                "1.8/5",
            ),
        ],
        className="row",
    ),


##########################
###### Charts ############
##########################

    html.Div(
        [
            html.Div(
                [
                    html.P("Pays principaux"),

                    dcc.Graph(
                        id="countries",
                        config=dict(displayModeBar=False),
                        #style={"height": "95%", "width": "98%"},
                    ),

                ],
                className="six columns chart_div",
            ),

            html.Div(
                [
                    html.P("Sentiments des clients"),
                    
                    dcc.Graph(
                        id="feelings",
                        config=dict(displayModeBar=False),
                       # style={"height": "89%", "width": "98%"},
                    ),
                ],
                className="six columns chart_div"
            ),
        ],
        className="row",
        style={"marginTop": "5px"},
    ),


    
]


##### callBack country chart
@app.callback(
    Output(component_id='countries', component_property='figure'),
    [Input(component_id='network_dropdown', component_property='value')])
def update_pie(selected_item):
    global file

    try:
    	
    	f = open(temp_country_file, 'r')
    	country_list = f.readline().split(',')[:-1]
    	value_list = f.readline().split(',')[:-1]
    	for k in range(len(value_list)):
    		value_list[k]=float(value_list[k])

    except:
    	print('Cannot read file :'+temp_country_file)
    	country_list,value_list = AnalyseNationnalites(file)

    

    updated_chart = go.Pie(labels=country_list[:10],values=value_list[:10],hoverinfo='label+percent+value')
    return {
        'data': [updated_chart]
    }

##### callBack feelingq chart
@app.callback(
    Output(component_id='feelings', component_property='figure'),
    [Input(component_id='network_dropdown', component_property='value')])
def update_pie(selected_item):
    global file
    labels_list = ["Positif", "Négatif", "Neutre"]
    colors=["green", "red", "grey"]

    


    if selected_item == 'TW':
    	value_list = PositiveNegative(file)

    elif selected_item == 'YT':
    	value_list = retrieve_emotions(3)[1]

    elif selected_item == 'FB':
    	try:
    		f = open(temp_sent_FB_file, 'r')
    		value_list = f.readline().split(',')[:-1]
    		for k in range(len(value_list)):
    			value_list[k]=float(value_list[k])

    	except:
    		print('Cannot read file : '+temp_sent_FB_file)
    		value_list = PositiveNegativeFB()


    elif selected_item == 'G':
    	try:
    		f = open(temp_sent_FB_file, 'r')
    		value_list_FB = f.readline().split(',')[:-1]
    		for k in range(len(value_list_FB)):
    			value_list_FB[k]=float(value_list_FB[k])

    	except:
    		print('Cannot read file : '+temp_sent_FB_file)
    		value_list_FB = PositiveNegativeFB()

    	value_list = [PositiveNegative(file)[x] + retrieve_emotions(3)[1][x] + value_list_FB[x] for x in range(3)]

    
    
    updated_chart = go.Pie(labels=labels_list,values=value_list,marker=dict(colors=colors),hoverinfo='label+percent')
    return {
        'data': [updated_chart]
    }



