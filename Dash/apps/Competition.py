# -*- coding: utf-8 -*-
import math
import json

import pandas as pd
import flask
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
from plotly import graph_objs as go
from func.ReturnChannelStat import *

from app import app, indicator, indicatorValue

###### Twitter functions
from func.TW_func import AnalyseNationnalites, PositiveNegative, TotalVisibilité
from func.RecuperationDonneeTwitter import *
from func.ReturnLikability import ReturnLikability

#PAth of the file to analise
file = 'DataProcessed/TableTweetsAirFranceTraite.csv'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

layout = [


#### First Row

    html.Div(
        [
            html.Div(   #Displays the 3 graphs side by side
                dcc.Graph(           #graph for Youtube
                    id='subscribers',
                    figure={
                        'data': [
                            {'x': [1], 'y': [int(return_stats(1)[1])], 'type': 'bar', 'name': u'Air France', 'marker':{'color': '#032653'}},
                            {'x': [1], 'y': [int(return_stats(3)[1])], 'type': 'bar', 'name': 'British Airways', 'marker':{'color': '#DF0101'}},
                            {'x': [1], 'y': [int(return_stats(2)[1])], 'type': 'bar', 'name': 'Lufthansa', 'marker':{'color': '#f0af00'}},
                        ],
                        'layout': {
                            'title': 'Subscribers on Youtube'
                        }
                    }
                ),
                className="one-third column",
                style={"marginBottom": "10"},
            ),
            html.Div(
                dcc.Graph(        #Graph for Facebook
                    id='likes',
                    figure={
                        'data': [
                            {'x': [1], 'y': [7421457], 'type': 'bar', 'name': u'Air France', 'marker':{'color': '#032653'}},
                            {'x': [1], 'y': [3128532], 'type': 'bar', 'name': 'British Airways', 'marker':{'color': '#DF0101'}},
                            {'x': [1], 'y': [3770366], 'type': 'bar', 'name': 'Lufthansa', 'marker':{'color': '#f0af00'}},
                        ],
                        'layout': {
                            'title': 'Number of likes on Facebook'
                        }
                    }
                ),
                className="one-third column",
            ),
            html.Div(
                dcc.Graph(         #Graph for Twitter
                id='followers',
                figure={
                    'data': [
                        {'x': [1], 'y': [int(NumberOfFollowers('airfrance'))], 'type': 'bar', 'name': 'Air France', 'marker':{'color': '#032653'}},
                        {'x': [1], 'y': [int(NumberOfFollowers('British_Airways'))], 'type': 'bar', 'name': u'British Airways', 'marker':{'color': '#DF0101'}},
                        {'x': [1], 'y': [int(NumberOfFollowers('lufthansa'))], 'type': 'bar', 'name': u'Lufthansa', 'marker':{'color': '#f0af00'}},
                    ],
                    'layout': {
                        'title': 'Followers on Twitter'
                    }
                }
            ),
                className="one-third column",
            ),

          




        ],
        className="row",
        style={"marginTop": "5px"}),
    html.Div(
          	[
                dcc.Graph(
                    id='likeability',
                    figure={
                        'data': [
                            go.Bar(
                            	x=[float(ReturnLikability(3)), float(ReturnLikability(2)), float(ReturnLikability(1))],
            					y=['AirFrance', 'British Airways', 'Lufthansa'],
            					orientation = 'h',
            					marker = dict(color = ['#032653','#DF0101','#f0af00'])
								)
                        ],
                        'layout': {
                            'title': 'Likeability (nombre de likes pour 1 dislike)'
                        }
                    }
                ),
                ],
                #className="one-third column",
                style={"marginBottom": "10"},
            ),
        
        
#### Second row

    html.Div(         #Displays wordcloud (hosted online)
         [
             html.Div([       #AirFrance wordcloud
                html.Img(src='https://i.postimg.cc/G3VJXgrP/dash-AFcomp.png',height="80%",style={"float":"left","height":"80%","marginTop": "6px"})
                 ], className='one-third column', style = {'width': '30%'}),
             html.Div([     #BritishAirways wordcloud
                html.Img(src='https://i.postimg.cc/wMCX3MF4/dash-BAcomp.png',height="80%",style={"float":"left","height":"80%","marginTop": "6px"}),
                ], className='one-third column', style = {'width': '30%'}),
             html.Div([      #Lufthansa wordcloud
                html.Img(src='https://i.postimg.cc/QMzF6YwG/dash-LUcomp.png',height="80%",style={"float":"left","height":"80%","marginTop": "6px"})
                ], className='one-third column', style = {'width': '30%','marginLeft':'5px'})

                ],className="row",
                style = {"marginTop": "5px"}
                )




        ]
 #

        #



