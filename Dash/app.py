import math

import pandas as pd
import flask
import dash
import dash_core_components as dcc
import dash_html_components as html
import dateutil.parser


server = flask.Flask(__name__)
app = dash.Dash(__name__, server=server)
app.config.suppress_callback_exceptions = True






#returns top indicator div
def indicator(color, text, id_value):
    '''Automatically creates a html Div with an "indicator" inside. 
    :param arg1: color, a string with hexa code of color
    :param arg2: text, the title of the indicator
    :param arg3: id_value, the id you want to use, of the object (to update it)
                
    :return: the object = the structure including the div and the texts in the correct style format. 
    ''' 
    
    return html.Div(
        [
            
            html.P(
                text,
                className="twelve columns indicator_text"
            ),
            html.P(
                id = id_value,
                className="indicator_value"
            ),
        ],
        className="four columns indicator",
        
    )

def indicatorValue(color, text, id_value, value):
    '''Automatically creates a html Div with an "indicator" inside, displaying a fixed value. 
    :param arg1: color, a string with hexa code of color
    :param arg2: text, the title of the indicator
    :param arg3: id_value, the id you want to use, of the object (to update it)
    :param arg4: value, the value you want to display in the indicator
                
    :return: the object = the structure including the div and the texts in the correct style format. 
    
    '''
    return html.Div(
        [
            
            html.P(
                text,
                className="twelve columns indicator_text"
            ),
            html.P(
                value,
                id = id_value,
                className="indicator_value"
            ),
        ],
        className="four columns indicator",
        
    )

def TweetDisplay(color, text, id_value, value):
    
    '''Automatically creates a Tweet display box. 
    :param arg1: color, a string with hexa code of color
    :param arg2: text, the title of the indicator
    :param arg3: id_value, the id you want to use, of the object (to update it)
    :param arg4: value, the text of the tweet
                
    :return: the object = the structure including the div and the texts in the correct style format. 
    '''
    return html.Div(
        [
            
            html.P(
                text,
                className="twelve columns indicator_text"
            ),
            html.P(
                value,
                id = id_value,
                className="indicator_value",
                style={'fontSize': '18px', 'color':'#0277B2'},
            ),
            
        ],
        className="twelve columns indicator",style = {'height':'20%'},
        
    )