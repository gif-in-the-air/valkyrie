
# coding: utf-8

def NombresPropres(nombre):
    ''' In order to get clean number easy to read (separation between package of 3 digits)
    It takes in argument the number
    and returns it with the separations (just for style)'''
    nombreV2=''
    i=0
    for k in range(len(nombre)-1,-1,-1):
        if i<3:
            i+=1
            nombreV2+=nombre[k]
        else:
            i=1
            nombreV2=nombreV2+' '+nombre[k]
    return nombreV2[::-1]


def return_stats(ch):
    """This file is used to return stats on a channel given a choice of required channel"""
    import googleapiclient
    from googleapiclient.discovery import build 
    from googleapiclient.errors import HttpError 
    from oauth2client.tools import argparser
    import pandas as pd 
    import matplotlib as plt

    #Set up your developper account credentials
    DEVELOPER_KEY = "AIzaSyCYvw44pH9pgIX5z3IVONFDv8d_kUlSUMc" 
    YOUTUBE_API_SERVICE_NAME = "youtube"
    YOUTUBE_API_VERSION = "v3"

    youtube = build( YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,  developerKey=DEVELOPER_KEY )

    #Define the Channel IDs for each choice
    AFC = "UCt6_7e1yAZxhn5E2ojiZA6w"
    LHR = 'UCSeWBJOEs5ICRDy5Iw_mdFg'
    BA = 'UCSs-N1quBfssFcYyqm6V_oA'
    if ch==1:
        CID=AFC
    elif ch==2:
        CID=LHR
    else:
        CID=BA
    #Obtain response for each channel
    response = youtube.channels().list(part='snippet,statistics', id=CID).execute()
    Views = response['items'][0]['statistics']['viewCount'] #Number of views for entire channel
    Subscribers = response['items'][0]['statistics']['subscriberCount'] #Number of subscribers
    Videos = response['items'][0]['statistics']['videoCount'] #Number of videos
    return (Views,Subscribers, Videos)


