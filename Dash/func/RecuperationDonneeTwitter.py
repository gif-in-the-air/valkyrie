# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 15:20:32 2018

@author: Pierre
"""
import tweepy           # To consume Twitter's API

Count= 1000   #Number of tweets to analyse
## Consumer keys:
#consumer_key   = 'UE3krUzgiq6iQKSrNF7pr34vy' 
#consumer_secret = 'EGqrQg0A8A110OtUy21BcjFFQXvfAX9bEdt3jVCP5bIRCtEsEG' 
## Access keys:
#access_token  = '2389290380-E4bEWNXXZIGY2wpVMwIkB3pwiu9XWzCcWAxVcSx' 
#access_token_secret = 'ud03EbuBIgpwNAfBGGxbaTQqF3mSABqOfq0IvieWWjsQF' 

# Consumer keys:
consumer_key   =  '8AxGWjXDcVm7H6kXUYlqWXbpU'
consumer_secret =  'tycdglZuA5QINPSc60Wv6TQComBBzhzgyB8FJlpdmgsB2Lim85'
# Access keys:
access_token  =  '1063356336602267648-Gqw1x9Ud8cgHFKRtMaAA61GVKY4oX3'
access_token_secret = 'jk2O401GuQMhEBcq9XHk11MGxSdPQ7yIdaTX0eNUiswUU'
#create our API object

# Creating the authentication object
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
# Setting your access token and secret
auth.set_access_token(access_token, access_token_secret)
# Creating the API object while passing in auth information
api = tweepy.API(auth)




def TweetsfromASpecificUser(UserName): 
    '''For a Twitter account, returns its 1000 last tweets (only the text)
    The function takes in argument : the username of the Twitter account
    The function returns the text of the tweets in a list (of 1000 elements)'''
    

    # Calling the user_timeline function with our parameters
    results = tweepy.Cursor(api.user_timeline, #function wich gets all the information about a tweet of a username
    id=UserName, #UserName of the Twitter account
    tweet_mode='extended', #in order to get all the text of the tweet
    ).items(1000) #items(1000) in order to get 1000 tweets
    
    # Creating the list of tweets:
    ListeTweet=[]
    for tweet in results:
       ListeTweet.append(tweet.full_text) #Appends the text of each tweet to our list
    return ListeTweet   #List

def TweetsUsingAKeyword(Keyword): 
    '''For a keyword, returns the 1000 last tweets containing this keyword (only the text)
    The function takes in argument : the keyword
    The function returns lists, the first is the list of tweet (only text), 
    the second is the list of ID, 
    the third is the list of laction, 
    the forth is the list of the number of retweet 
    and the last the list of number of fav (of 1000 elements)'''
    
    
    # Calling the user_timeline function with our parameters and getting all the informations about the 1000 tweets (in english containing the keyword)
    # Créer la liste des tweets
    results = tweepy.Cursor(api.search, q=Keyword, tweet_mode='extended', lang='en').items(Count)
    ListeRetweetCount=[] #crating the list of number of retweet for each tweet
    ListeFavoriteCount=[] #crating the list of number of fav for each tweet
    ListeTweet=[]  # Creating the list of tweets (only text)
    ListeID=[] # Creating the list of the ID of the tweets
    ListeLocation=[] # Creating the list of the location of the tweets
    for tweet in results:
       ListeTweet.append(tweet.full_text)
       ListeID.append(tweet.id)
       ListeLocation.append(tweet.user.location)
       ListeRetweetCount.append(tweet.retweet_count)
       ListeFavoriteCount.append(tweet.favorite_count)
    return ListeTweet, ListeID, ListeLocation, ListeRetweetCount, ListeFavoriteCount #Lists

def NumberOfFollowers(UserID):
    '''The function enables you to get the number of followers of a Twitter Account
    It takes in argument the UserID 
    and returns the number of followers as a float'''
    
    results = api.get_user(UserID)
    return results.followers_count  #Float

def NationalityDesFollowers(UserID):
    '''The function gets the nationality of 1000 followers
    It takes in argument the UserID
    and returns the Nationality of the followers in a list'''
    
    results=tweepy.Cursor(api.followers, user_id=UserID).items(Count)
    Nationalités=[] #Creating the list
    for k in range (0,len(results)):
        if results[k].location != '':
            Nationalités.append(results[k].location) #append the elements of the list
    return Nationalités   #List

def NationalityDesTwittersMen(Keyword):
    '''The function gets the nationality of the last 1000 twittermen who tweeted with the keyword Keyword
    It takes in argument the keyword
    and returns in a list the nationalities'''
    
    results=tweepy.Cursor(api.search, q=Keyword, rpp=20).items(Count) #getting all the information
    ListeNationalités=[]
    for tweet in results:
        ListeNationalités.append(tweet.user.location)
    return ListeNationalités  #List
