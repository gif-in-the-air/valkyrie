
# coding: utf-8


def retrieve_emotions(data, ch):
    """This function returns the emotions of a CSV file for each functionality involved"""

    # 0 - AirFrance_Twitter
    # 1 - British_Twitter
    # 2 - Lufthansa_Twitter
    # 3 - AirFrance_YouTube
    # 4 - British_YouTube
    # 5 - Lufthansa_YouTube

    import xlrd
    workbook = xlrd.open_workbook('C:/Users/niraj/Downloads/CW/EC.xlsx') #File containing classification of emotions

    worksheet = workbook.sheet_by_index(0)  
    first_row = [] # The row where we stock the name of the column
    for col in range(worksheet.ncols):
        first_row.append( worksheet.cell_value(0,col) )
    # transform the workbook to a list of dictionnary

    data =[]
    for row in range(1, worksheet.nrows):
        elm = {}
    for col in range(worksheet.ncols):
        elm[first_row[col]]=worksheet.cell_value(row,col)
    data.append(elm)


    returndata = [ data[ch]['Neutral'] , data[ch]['Joy'], data[ch]['Anger'], data[ch]['Surprise'], data[ch]['Sadness'],data[ch]['Fear'], data[ch]['Disgust']]
    returnlabels =['Neutral', 'Joy', 'Anger', 'Surprise', 'Sadness', 'Fear', 'Disgust']
    
    return(returndata,returnlabels)
                


