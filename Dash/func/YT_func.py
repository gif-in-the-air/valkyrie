
# coding: utf-8




import xlrd

import googleapiclient
from googleapiclient.discovery import build 
from googleapiclient.errors import HttpError 
from oauth2client.tools import argparser
import pandas as pd 
import matplotlib as plt

DEVELOPER_KEY = "AIzaSyCYvw44pH9pgIX5z3IVONFDv8d_kUlSUMc" 
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

youtube = build( YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,  developerKey=DEVELOPER_KEY )


# In[1]:



AFC = "UCt6_7e1yAZxhn5E2ojiZA6w"
LHR = 'UCSeWBJOEs5ICRDy5Iw_mdFg'
BA = 'UCSs-N1quBfssFcYyqm6V_oA'



YT_data_file='DataProcessed/YoutubeSentiments.xlsx'

def retrieve_emotions(ch):
    ''' ch value :
    # 0 - AirFrance_Twitter
    # 1 - British_Twitter
    # 2 - Lufthansa_Twitter
    # 3 - AirFrance_YouTube
    # 4 - British_YouTube
    # 5 - Lufthansa_YouTube'''

    workbook = xlrd.open_workbook(YT_data_file)

    worksheet = workbook.sheet_by_index(0)
    first_row = [] # The row where we stock the name of the column
    for col in range(worksheet.ncols):
        first_row.append( worksheet.cell_value(0,col) )
    # tronsform the workbook to a list of dictionnary

    data =[]
    for row in range(1, worksheet.nrows):
        elm = {}
        for col in range(worksheet.ncols):
            elm[first_row[col]]=worksheet.cell_value(row,col)
        data.append(elm)
    returndata = [ data[ch]['Neutral'] , data[ch]['Joy'], data[ch]['Anger'], data[ch]['Surprise'], data[ch]['Sadness'],data[ch]['Fear'], data[ch]['Disgust']]
    returnlabels =['Neutral', 'Joy', 'Anger', 'Surprise', 'Sadness', 'Fear', 'Disgust']
    posnegneutral=[returndata[1], returndata[0], returndata[2]+returndata[4]+returndata[6]]
    
    return returndata, posnegneutral
                


def NombresPropres(nombre):
    nombreV2=''
    i=0
    for k in range(len(nombre)-1,-1,-1):
        if i<3:
            i+=1
            nombreV2+=nombre[k]
        else:
            i=1
            nombreV2=nombreV2+' '+nombre[k]
    return nombreV2[::-1]



def return_stats(ch):
    if ch==1:
        CID=AFC
    elif ch==2:
        CID=LHR
    else:
        CID==BA
    response = youtube.channels().list(part='snippet,statistics', id=CID).execute()
    Views = response['items'][0]['statistics']['viewCount']
    Subscribers = response['items'][0]['statistics']['subscriberCount']
    Videos = response['items'][0]['statistics']['videoCount']
    return (Views,Subscribers, Videos)