
# coding: utf-8

# In[92]:


"""This Python file retrives comments from YouTube, processes it, builds the wordcloud, polarity, subjectivity and finally processes the comments into a CSV file ready to be processed by R for emotion classification"""

import googleapiclient
from googleapiclient.discovery import build 
from googleapiclient.errors import HttpError 
from oauth2client.tools import argparser
import pandas as pd 
import matplotlib as plt


# In[93]:


#Set up your google developper credentials
DEVELOPER_KEY = "AIzaSyCYvw44pH9pgIX5z3IVONFDv8d_kUlSUMc"  #Your developper keys
YOUTUBE_API_SERVICE_NAME = "youtube"  #Name of the google serive
YOUTUBE_API_VERSION = "v3" #Version of API used


# In[94]:


youtube = build( YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,  developerKey=DEVELOPER_KEY ) #The YouTube client to retrive data


# In[141]:


search_response = youtube.search().list(
 q= "British Airways" , ## Your search term stored in options below
 type= "channel", ## Type of media
 part="id,snippet", ## Search about parts you want to extract
 maxResults="1"## Your max-results stored in options below
).execute()


# In[250]:


LHR = 'UCSeWBJOEs5ICRDy5Iw_mdFg' #The channel ID of the required Clients, obtained from the result of youtube.search command
BA = 'UCSs-N1quBfssFcYyqm6V_oA'
AF= "UCt6_7e1yAZxhn5E2ojiZA6w"
search_response = youtube.search().list(q= "" #Search term, empty in this case as we have channel ID
                                        , type= "video", #Required media, either video/channel/etc. 
                                        part="snippet", #Which part of result to be retrieved 
                                        maxResults="50", #Max reults of 50 possile
                                        channelId=LHR,  #Required channel ID of the client
                                        order="viewCount" #In the order in which you want the results to be retrieved 
                                       ).execute()



# In[251]:


search_videos = []

# Merge video ids obtained from search to be used 
for search_result in search_response.get('items', []):
    search_videos.append(search_result['id']['videoId']) 
    
video_ids = ','.join(search_videos)


count=0
dataObtained={}
names=[]

for i in range(len(search_videos)):
    try:
        t={}
        results = youtube.commentThreads().list(
            part="snippet",
            videoId=search_videos[i],maxResults='100',
            textFormat="plainText"
          ).execute()
        
        
        #print ("This is video number" , i+1)
        #print (len(results["items"]))
        for item in results["items"]:
            comment = item["snippet"]["topLevelComment"]
            author = comment["snippet"]["authorDisplayName"]
            text = comment["snippet"]["textDisplay"]
            #print ("Author: ", author, "\nText :", text, "\n\n")
            temp[k] = text
            count+=1
            dataObtained.update(t)
            #data_youtube[k].append(d)        
    except:
        print ("Comments disabled")


# In[171]:


#Set up google translation API

import os,time 
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "C:/Users/niraj/Downloads/MyCodingWeek-3bd33d179218.json"
from google.cloud import translate
client = translate.Client()

#The default arguement is automatic translation into English which was used

dataTrans={}
for i in range(len(d)):
    a=client.translate(d[i])
    dataTrans[i] = a['translatedText']
    time.sleep(0.1) #Sleep was used to limit number of request per second
    print ((i/len(d))*100) #Keep track of progress of translation


# In[181]:


data=dataTrans

#Remove all punctuations
import re
for i in range(len(data)):
    data[i] = re.sub(r'[^\w\s]','',datatrans[i])
    


# In[183]:


import nltk
from nltk.corpus import stopwords
import pandas as pd 
from textblob import TextBlob

#Remove all numbers
for i in range(len(data)):
    data[i] = re.sub(" \d+", " ", data[i])
   

#Remove all stopwords
stopword = stopwords.words('english')
    
for i in range(len(data)):
    querywords = data[i].split()
    resultwords  = [word for word in querywords if word.lower() not in stopword]
    data[i] = ' '.join(resultwords)


# In[184]:


#Remove URL if any
for i in range(len(data)):
    data[i] = re.sub(r'^https?:\/\/.*[\r\n]*', '', data[i], flags=re.MULTILINE)


# In[185]:


#Lemmatise the data
from textblob import Word
for i in range(len(data)):
    resultwords  = [Word(y).lemmatize() for y in data[i].split()]
    data[i] = ' '.join(resultwords) 
#Stemming of data    
from nltk.stem import PorterStemmer
st = PorterStemmer()
for i in range(len(data)):
    resultwords  = [st.stem(y) for y in data[i].split()]
    data[i] = ' '.join(resultwords) 


# In[186]:


#Retrieve all the subjectivity and polarity of the comments

dataSub=[]
dataPol=[]

for i in range(len(data)):
    dataSub.append(TextBlob(data[i]).sentiment[1])
    dataPol.append(TextBlob(data[i]).sentiment[0])
    
def PositiveNegative(data):
    """This function returns the number of each neutral, negative and postive comments"""
   pos=0
   neg=0
   neut=0
   for i in data:
       if i<0:
           neg+=1
       elif i>0:
           pos+=1
       else:
           neut+=1
   return [pos,neg,neut]

pos,neg,neut= PositiveNegative(datapol)  


# In[178]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'nbagg #Show image inline')
plt.hist(dataPol, bins='auto') #Histogram of Polarity with bins sets to Automatic
plt.show()


# In[191]:


#Build a wordcloud of most emotions and words

from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
stopwords = set(STOPWORDS)
import csv


def show_wordcloud(data, title = None):
"""Builds the wordcloud and prints and image of the most recurring words"""    
        wordcloud = WordCloud(
        background_color='white', #Preferred BG color
        stopwords=stopwords, #Words to be filtered
        max_words=100,  #MAx no. of words
        max_font_size=40,  #Max font size 
        scale=3,
        random_state=1 # chosen at random by flipping a coin; it was heads
    ).generate(str(data))
    

        fig = plt.figure(1, figsize=(12, 12))
        plt.axis('off')
        if title: 
            fig.suptitle(title, fontsize=20)
            fig.subplots_adjust(top=2.5)

        plt.imshow(wordcloud)
        plt.show()

    
get_ipython().run_line_magic('matplotlib', 'nbagg      #To show the figure inline on Jupyter')
data2 = datafil
show_wordcloud(data=data2)
plt.savefig('imageLA.jpg') #SAve the image for presentation


# In[137]:


#Write the processed comments into a CSV file to be processed by R for emotions
with open('youtube_comments_processed_LA_relevance.csv','w') as fout:
    writer=csv.writer(fout)    
    for row in zip(data.values()):
        row=[s.encode('utf-8') for s in row]
        writer.writerows([row])



#Building the 'Likability' factor

video_response = youtube.videos().list(id=video_ids, part='statistics').execute() #Retrive stats on video based on IDs obtained before
likes=[] #List of likes for each video
dislikes=[] #Dislikes of each video
for i in range(len(video_response['items'])):
    likes.append(video_response['items'][i]['statistics']['likeCount'])
    dislikes.append(video_response['items'][i]['statistics']['dislikeCount'])


# In[253]:


#Define likability factor as Number of Likes divided by number of dislikes

fdiv = [float(int(ai))/(int(bi)) for bi,ai in zip(dislikes,likes) if int(bi)>0]
likability =  (sum(fdiv)/len(likes))






