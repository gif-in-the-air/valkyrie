# coding: utf-8

def ReturnLikability(ch):
    """This Python function returns the average likabilty of each channel. Use ch=1 for Lufthansa, 2 for British Airways and 3 for Air France"""

    import googleapiclient
    from googleapiclient.discovery import build 
    from googleapiclient.errors import HttpError 
    from oauth2client.tools import argparser
    import pandas as pd 
    import matplotlib as plt
    #Set up your google developper credentials
    DEVELOPER_KEY = "AIzaSyCYvw44pH9pgIX5z3IVONFDv8d_kUlSUMc"  #Your developper keys
    YOUTUBE_API_SERVICE_NAME = "youtube"  #Name of the google serive
    YOUTUBE_API_VERSION = "v3" #Version of API used
    youtube = build( YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,  developerKey=DEVELOPER_KEY ) #The YouTube client to retrive data

    LHR = 'UCSeWBJOEs5ICRDy5Iw_mdFg' #The channel ID of the required Clients, obtained from the result of youtube.search command
    BA = 'UCSs-N1quBfssFcYyqm6V_oA'
    AF= "UCt6_7e1yAZxhn5E2ojiZA6w"
    
    if ch==1:
        CID=LHR
    elif ch==2:
        CID=BA
    else:
        CID=AF
    search_response = youtube.search().list(q= "" #Search term, empty in this case as we have channel ID
                                            , type= "video", #Required media, either video/channel/etc. 
                                            part="snippet", #Which part of result to be retrieved 
                                            maxResults="50", #Max reults of 50 possile
                                            channelId=CID,  #Required channel ID of the client
                                            order="viewCount" #In the order in which you want the results to be retrieved 
                                           ).execute()



    # In[251]:


    search_videos = []

    # Merge video ids obtained from search to be used 
    for search_result in search_response.get('items', []):
        search_videos.append(search_result['id']['videoId']) 

    video_ids = ','.join(search_videos)




    video_response = youtube.videos().list(id=video_ids, part='statistics').execute() #Retrive stats on video based on IDs obtained before
    likes=[] #List of likes for each video
    dislikes=[] #Dislikes of each video
    for i in range(len(video_response['items'])):
        likes.append(video_response['items'][i]['statistics']['likeCount'])
        dislikes.append(video_response['items'][i]['statistics']['dislikeCount'])

    fdiv = [float(int(ai))/(int(bi)) for bi,ai in zip(dislikes,likes) if int(bi)>0]
    likability =  (sum(fdiv)/len(likes))
    return likability