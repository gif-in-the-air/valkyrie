# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 15:33:55 2018

@author: valen
"""
##bibliothèques
from oauth2client import service_account
from googleapiclient.discovery import build
import json
import pandas as pd
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import nltk
from nltk.corpus import stopwords
import pandas as pd 
from textblob import TextBlob
#import google.cloud
import time
import re
from textblob import Word
import matplotlib.pyplot as plt
from nltk.stem import PorterStemmer


##import data
client_credentials = json.load(open("static/FB_Keys.json"))
credentials_token = service_account._JWTAccessCredentials.from_json_keyfile_dict(client_credentials)
facebook_af = pd.read_csv("DataProcessed/TableFacebookAirFrance.csv")
facebook_lu= pd.read_csv("DataProcessed/TableFacebookLufthansa.csv")
facebook_ba= pd.read_csv("DataProcessed/TableFacebookBritishAirways.csv")
temp_sent_FB_file = 'DataProcessed/sent_FB.tmp'

##translation
def translate_text(text,target='en'):
    """
    Target must be an ISO 639-1 language code.
    https://cloud.google.com/translate/docs/languages
    """
    translate_client = translate.Client.from_service_account_json("Dash/static/FB_Keys.json")
    result = translate_client.translate(
        text,
        target_language=target)
    return result['translatedText']
    

def FacebookAnalysis():
    """
    traduit les commentaires en anglais, puis nettoie les commentaires
    renvoie la polarité de tous les commentaires
    """

    facebook_substract=facebook_af[0:1000] ## il y avait trop de donnees, cela mettait trop de temps à charger, on a du choisir 1000 commentaires
    facebook_substract['translated_commentaires']= [translate_text(str(facebook_substract['object_link.connections.comments.message'][i]).encode().decode('utf-8'),target='en') for i in facebook_substract.index]
    facebook_lu['translated_commentaires']= [translate_text(str(facebook_lu['object_link.connections.comments.message'][i]).encode().decode('utf-8'),target='en') for i in facebook_lu.index]
    facebook_ba['translated_commentaires']= [translate_text(str(facebook_ba['object_link.connections.comments.message'][i]).encode().decode('utf-8'),target='en') for i in facebook_ba.index]


    ##pre processing
    facebook_substract['comcleared'] = facebook_substract['translated_commentaires'].str.replace('[^\w\s]', '')
    facebook_lu['comcleared'] = facebook_substract['translated_commentaires'].str.replace('[^\w\s]', '')
    facebook_ba['comcleared'] = facebook_ba['translated_commentaires'].str.replace('[^\w\s]', '')

    facebook_substract['comcleared'] = facebook_substract['comcleared'].apply(lambda x: " ".join([x for x in x.split() if not x.isdigit()]))
    facebook_lu['comcleared'] = facebook_lu['comcleared'].apply(lambda x: " ".join([x for x in x.split() if not x.isdigit()]))
    facebook_ba['comcleared'] = facebook_ba['comcleared'].apply(lambda x: " ".join([x for x in x.split() if not x.isdigit()]))

    nltk.download('stopwords')
    stopword = stopwords.words('english')
    stopwords = set(STOPWORDS)
    nltk.download('wordnet')

    facebook_substract['comcleared'] = facebook_substract['comcleared'].apply(lambda x: " ".join(x for x in x.split() if x not in stopword))
    facebook_lu['comcleared'] = facebook_lu['comcleared'].apply(lambda x: " ".join(x for x in x.split() if x not in stopword))
    facebook_ba['comcleared'] = facebook_ba['comcleared'].apply(lambda x: " ".join(x for x in x.split() if x not in stopword))

    facebook_substract['comcleared'] = [re.sub(r"http\S+", '',facebook_substract['comcleared'][i]) for i in facebook_substract.index]
    facebook_lu['comcleared'] = [re.sub(r"http\S+", '',facebook_lu['comcleared'][i]) for i in facebook_lu.index]
    facebook_ba['comcleared'] = [re.sub(r"http\S+", '',facebook_ba['comcleared'][i]) for i in facebook_ba.index]



    facebook_substract['comcleared'] = facebook_substract['comcleared'].apply(lambda x: " ".join([Word(y).lemmatize() for y in x.split()]))
    facebook_lu['comcleared'] = facebook_lu['comcleared'].apply(lambda x: " ".join([Word(y).lemmatize() for y in x.split()]))
    facebook_ba['comcleared'] = facebook_ba['comcleared'].apply(lambda x: " ".join([Word(y).lemmatize() for y in x.split()]))


    st = PorterStemmer()
    facebook_substract['comcleared'] = facebook_substract['comcleared'].apply(lambda x: " ".join([st.stem(y) for y in x.split()]))
    facebook_lu['comcleared'] = facebook_lu['comcleared'].apply(lambda x: " ".join([st.stem(y) for y in x.split()]))
    facebook_ba['comcleared'] = facebook_ba['comcleared'].apply(lambda x: " ".join([st.stem(y) for y in x.split()]))

    facebook_substract['polarity'] = facebook_substract['comcleared'].apply(lambda x : TextBlob(x).sentiment[0])
    facebook_lu['polarity'] = facebook_lu['comcleared'].apply(lambda x : TextBlob(x).sentiment[0])
    facebook_ba['polarity'] = facebook_ba['comcleared'].apply(lambda x : TextBlob(x).sentiment[0])

    return facebook_substract['polarity']

##fonctions

def PositiveNegativeFB():
    """
    sert à compter le nombre d'occurences de polarité positives, négatives et neutres
    renvoie ces nombres sous le forme d'une liste
    """

    data = FacebookAnalysis()

    pos=0
    neg=0
    neut=0
    for polarity in data:
        if polarity<0:
            neg+=1
        elif polarity>0:
            pos+=1
        else:
            neut+=1

    with open(temp_sent_FB_file, 'w') as f:
      
        f.write(pos+',')
        f.write(neg+',')
        f.write(neut)

    return [pos,neg,neut]



# PositiveNegative(facebook_substract['polarity']) ##renvoie [value_pos,value_neg,value_neutre]
# PositiveNegative(facebook_lu['polarity'])
# PositiveNegative(facebook_ba['polarity'])

