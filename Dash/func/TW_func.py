# TWITTER DATA ANALYSIS


# General: import libraries
import tweepy           # To consume Twitter's API
import pandas as pd     # To handle data
import numpy as np      # For number computing
from geopy.geocoders import Nominatim
# Consumer keys:
consumer_key   = 'UE3krUzgiq6iQKSrNF7pr34vy'
consumer_secret = 'EGqrQg0A8A110OtUy21BcjFFQXvfAX9bEdt3jVCP5bIRCtEsEG'
# Access keys:
access_token  = '2389290380-E4bEWNXXZIGY2wpVMwIkB3pwiu9XWzCcWAxVcSx'
access_token_secret = 'ud03EbuBIgpwNAfBGGxbaTQqF3mSABqOfq0IvieWWjsQF'


#create our API object

# Creating the authentication object
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
# Setting your access token and secret
auth.set_access_token(access_token, access_token_secret)
# Creating the API object while passing in auth information
api = tweepy.API(auth)

def locationList(file):
   '''   returns the list of location of tweets from a csv file   '''
   res = []
   tweets = pd.read_csv(file, sep =",")
   for x in tweets['Location']:
       res.append(x)
   return res


def CodeNationnaliteDesFollowers(UserID):
	# Data from API
   #results=api.followers(user_id=UserID)
   locations = locationList()

   #Finding the country with geopy
   geolocator = Nominatim(user_agent="Valkyrie")

   Nationalites=[]
   pays=[]
   codes=[]
   for k in range (0,len(results)):
   		loc=results[k].location
   		
   		if loc != '':
   			try:  #Ca permet de supprimer les locatiosn de merde (du genre ici, wolrdwide etc)
	   			full = geolocator.geocode(loc, language='en').raw['display_name']
	   			full_list = full.split(', ')
	   			country = str(full_list[-1])
	   			codes.append(correspondance[correspondance.COUNTRY==country].CODE.item())

	   		except:
	   			pass
   return codes




def PaysDesTweets(file):
  '''Donne une liste des pays des gens qui ont tweete sur le fichier
  Entree : file le fichier de donnees traitees
  Sortie : liste des pays'''

  locations = locationList(file)[0:100]
  geolocator = Nominatim(user_agent="Valkyrie")
  pays=[]

  for k in range (0,len(locations)):
      loc_du_tweet=locations[k]
      
      if loc_du_tweet != '':
        try:  #Ca permet de supprimer les locations de merde (du genre ici, wolrdwide etc)

          full_data = geolocator.geocode(loc_du_tweet, language='en').raw['display_name']
          full_list = full_data.split(', ')
          country = str(full_list[-1])
          pays.append(country)

        except:
          pass



  return pays

def AnalyseNationnalites(file):
  '''Renvoie les pays représentés et le nombre de tweets de chauqe pays
  Entree : fichier de tweets traites
  Sortie, un t uple de listes de labels et values a utiliser pour un pie chart'''
  pays_brut = PaysDesTweets(file)
  pays_distinct=[]
  nombre_tweets_pays=[]

  for pays in pays_brut:
    if pays not in pays_distinct:
      pays_distinct.append(pays)
      nombre_occurences = pays_brut.count(pays)
      nombre_tweets_pays.append(nombre_occurences)

  return pays_distinct, nombre_tweets_pays


def search_tweet_extract(search_words):
    ''' Arg : Mot clé à chercher dans les tweets. Crée un csv contenant les tweets et les paramètres et retourne le tableau panda'''
    # Return API with authentication:
    api = tweepy.API(auth) # Fill with auth  
    max_tweets = 1000 # max 1000
    searched_tweets = [status for status in tweepy.Cursor(api.search, q=search_words, tweet_mode = 'extended', lang = 'en').items(max_tweets)]
    data = pd.DataFrame()
    data['Tweets'] = np.array([tweet.full_text for tweet in searched_tweets])
    data['ID'] = np.array([tweet.id for tweet in searched_tweets])
    data['Location'] = np.array([tweet.user.location for tweet in searched_tweets])
    data['RT'] = np.array([tweet.retweet_count for tweet in searched_tweets])
    data['Fav'] = np.array([tweet.favorite_count for tweet in searched_tweets])
    data['Visibilite'] = np.array([tweet.user.listed_count for tweet in searched_tweets])
    data.to_csv("TableTweets"+search_words+".csv", encoding = 'utf-8')
    return data # Return the data.frame


def PolarityText(Text):
    TextTweet = TextBlob(Text)
    return TextTweet.sentiment.polarity #Float entre -1 et 1

def SubjectivityText(Text):
    TextTweet = TextBlob(Text)
    return TextTweet.sentiment.subjectivity #Float entre -1 et 1

def PolarityListeTweet(ListeTweet):
    ListePolarity=[]
    for k in range(0,len(ListeTweet)):
        ListePolarity.append(PolarityText(ListeTweet[k]))
    return ListePolarity #Liste de Float entre -1 et 1

def SubjectivityListeTweet(ListeTweet):
    ListeSubjectivity=[]
    for k in range(0,len(ListeTweet)):
        ListeSubjectivity.append(SubjectivityText(ListeTweet[k]))
    return ListeSubjectivity #Liste de Float entre -1 et 1

def sentiment(ListeTweet):
    ListeSubjectivity=[]
    ListePolarity=[]
    for k in range(0,len(ListeTweet)):
         ListeSubjectivity.append(SubjectivityText(ListeTweet[k]))
         ListePolarity.append(PolarityText(ListeTweet[k]))
    return ListePolarity, ListeSubjectivity



def PositiveNegative(file):
  '''data = pandas dataframe'''
  data = pd.read_csv(file, sep =",")
  pos=0
  neg=0
  neut=0
  for polarity in data['polarity']:
      if polarity<0:
          neg+=1
      elif polarity>0:
          pos+=1
      else:
          neut+=1
  return [pos,neg,neut]

def TweetLePlusPopulaire(file):
    data = pd.read_csv(file,sep=',')
    MaxRetweet=0
    indice=0
    for k in range (0,len(data['RT'])):
        if data['RT'][k]>MaxRetweet:
            MaxRetweet=data['RT'][k]
            indice=k
    return data.loc[indice]   #[2] pour le text        #[5] pour le nombre de retweet

def ListeEnCaractere(Liste):
    Caractere=''
    for k in range (0,len(Liste)):
        Caractere=Caractere+ ' ' + Liste[k]
    return Caractere #String

def CountWords(ListeTweet,word): #Compter le nombre de motes dans une liste de tweet
    token = TextBlob(ListeEnCaractere(ListeTweet))
    return token.words.count(word) #Float

def NumberOfFollowers(UserID):
    # Creating the API object while passing in auth information
    api = tweepy.API(auth)
    results = api.get_user(UserID)
    return results.followers_count  #Float

def TotalVisibilité(file):
    data=pd.read_csv(file, sep=',')
    return data['Visibilite'].sum()
