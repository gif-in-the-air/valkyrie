# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 15:20:34 2018

@author: Pierre
"""
from textblob import TextBlob
import re
from string import digits
#importing the package to analyse our data :)

def PolarityText(Text):
    '''The function analyses a string and returns its polarity (float between -1 and +1)'''
    
    TextTweet = TextBlob(Text)
    return TextTweet.sentiment.polarity #Float between -1 and 1

def SubjectivityText(Text):
    '''The function analyses a string and returns its subjectivity (float)'''
    TextTweet = TextBlob(Text)
    return TextTweet.sentiment.subjectivity #Float 

    


def PolarityListeTweet(ListeTweet):
    '''The function analyses the elements of a list and returns the polarity of each element in a list'''
    
    ListePolarity=[]
    for k in range(0,len(ListeTweet)):
        ListePolarity.append(PolarityText(ListeTweet[k]))
    return ListePolarity #List of Float between -1 et 1

def SubjectivityListeTweet(ListeTweet):
    '''The function analyses the elements of a list and returns the subjectivity of each element in a list'''
    ListeSubjectivity=[]
    for k in range(0,len(ListeTweet)):
        ListeSubjectivity.append(SubjectivityText(ListeTweet[k]))
    return ListeSubjectivity #List of Float 

def sentiment(ListeTweet):
    '''In order to do only one loop to analyse the sentiment of a tweet i merge Subjectivity and Polarity, 
    The function analyses the elements of a list and returns :
    - the subjectivity of each element in a list
    - the polarity of each element in a list'''
    
    ListeSubjectivity=[]
    ListePolarity=[]
    for k in range(0,len(ListeTweet)):
         ListeSubjectivity.append(SubjectivityText(ListeTweet[k]))
         ListePolarity.append(PolarityText(ListeTweet[k]))
    return ListePolarity, ListeSubjectivity #Lists



def PositiveNegative(data):
    '''In order to build our pie chart of positivity we had to get the proportion of positive tweets and negative tweets:
    The function takes in argument a dataframe named data which contains a column "Polarity"
    and returns in a list the number of positive tweet, negative tweet and the neutral tweets/ not analysed tweets'''
    
    pos=0
    neg=0
    neut=0
    for polarity in data['polarity']:
        if polarity<0:
            neg+=1
        elif polarity>0:
            pos+=1
        else:
            neut+=1
    return [pos,neg,neut]

def TweetLePlusPopulaire(data):
    '''In order to get the most popular tweet:
    The funcion takes in argument a dataframe named data which contains a column Number of Retweet:
    The function returns the ID, the text, the polarity, the subjectivity, the number of fav and the number of retweet of the most popular tweet'''
    
    MaxRetweet=0
    indice=0
    for k in range (0,len(data['RT'])):
        if data['RT'][k]>MaxRetweet:
            MaxRetweet=data['RT'][k]
            indice=k
    return data.loc[indice]   #[2] pour le text        #[5] pour le nombre de retweet

def ListeEnCaractere(Liste):
    '''In order to do our wordcloud we had to convert a list of 1000 tweets in only one string:
    The function takes in argument the list of tweets (only the text)
    and return it in only one string'''
    
    Caractere=''
    for k in range (0,len(Liste)):
        Caractere=Caractere+ ' ' + Liste[k] #we add each time the element of the list to our string
    return Caractere #String

def CountWords(ListeTweet,word): 
    '''Compter le nombre du mot choisis dans une liste de tweet:
    En argument : le mot choisis et la liste de tweets
    Retourne: le compteur de ce mot dans notre liste de tweet'''
    
    token = TextBlob(ListeEnCaractere(ListeTweet))
    return token.words.count(word) #Float
    
    
    