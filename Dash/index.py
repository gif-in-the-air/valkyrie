# -*- coding: utf-8 -*-
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import flask
import plotly.plotly as py
from plotly import graph_objs as go
import math
from app import app, server
from apps import home, trends, alerts, Competition

app.layout = html.Div(
    [
        # header
        html.Div([

            html.Span(" ", className='app-title'),
            
            html.Div(
                html.Img(src='https://i.postimg.cc/Qt38SvGH/logo-dark.jpg',height="100%")
                ,style={"float":"left","height":"100%"}),

            html.Div(

            	html.Img(src='https://i.postimg.cc/bwnHt2zp/logo-air-france.jpg',height="100%"), 
            	style={"float":"right","height":"100%"})
            ],
            className="row header"
            ),

        # tabs
        html.Div([

            dcc.Tabs(
                id="tabs",
                style={"height":"20","verticalAlign":"middle"},
                children=[
                    dcc.Tab(label="Home", value="home"),
                    dcc.Tab(label="Trends", value="trends"),
                    dcc.Tab(id="alerts-tab",label="Alerts", value="alerts-tab"),
                    dcc.Tab(id="competition_tab", label="Competition Analysis", value="competition_tab")
                ],
                value="home",
            )

            ],
            className="row tabs_div"
            ),



        # Tab content
        html.Div(id="tab_content", className="row", style={"margin": "2% 3%"}),
        
        html.Link(href="https://use.fontawesome.com/releases/v5.2.0/css/all.css",rel="stylesheet"),
        html.Link(href="https://cdn.rawgit.com/plotly/dash-app-stylesheets/2d266c578d2a6e8850ebce48fdb52759b2aef506/stylesheet-oil-and-gas.css",rel="stylesheet"),
        html.Link(href="https://fonts.googleapis.com/css?family=Dosis", rel="stylesheet"),
        html.Link(href="https://fonts.googleapis.com/css?family=Open+Sans", rel="stylesheet"),
        html.Link(href="https://fonts.googleapis.com/css?family=Ubuntu", rel="stylesheet"),
        html.Link(href="https://cdn.rawgit.com/amadoukane96/8a8cfdac5d2cecad866952c52a70a50e/raw/cd5a9bf0b30856f4fc7e3812162c74bfc0ebe011/dash_crm.css", rel="stylesheet")
    ],
    className="row",
    style={"margin": "0%"},
)


#### Tabs callBack
@app.callback(Output("tab_content", "children"), [Input("tabs", "value")])
def render_content(tab):
    if tab == "home":
        return home.layout
    elif tab == "alerts-tab":
        return alerts.layout
    elif tab == "trends":
        return trends.layout
    elif tab == "competition_tab":
        return Competition.layout
    else:
        return home.layout

if __name__ == "__main__":
    app.run_server(debug=True)
