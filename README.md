# Valkyrie

A Python Dash dashboard to monitor _AirFrance's_ social media popularity. 

## Structure

* **_Data_** contient toutes les données (tweets etc) en format csv. 

* **_Dash_** contient les fichiers py pour le dashboard. 

    * **_func_** contient les fonctions de traitement des données. 
    * **_apps_** contient les layout de chaque onglet du Dash (Home, Trends, Alerts, Competiton Analysis)
    * **_DataProcessed_** contient toutes les données (tweets etc) en format csv traitées pour nos besoins ainsi que les fichiers temporaires d'analyse. 
    * **_static_** contient les css et les clés Facebook. 

## Modules nécessaires

### python

* math
* json
* pandas
* flask
* time
* os
* csv

### Dash

* dash
* plotly
* wordcloud
* matplotlib

### Analyse

* nltk
* numpy
* datetime
* oauth2client
* geopy
* google-cloud
* textblob
* re
* tweepy
* googleapiclient
* xlrd
